<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/edificios', 'BuildingController');

Route::resource('/estructura', 'BuildingItemsController');

Route::resource('/espacios', 'SpaceController');

Route::resource('/egresos', 'ExpenseController');

Route::resource('/asociaciones', 'ResidentUnityController');

Route::resource('/fondos', 'FundController');

Route::resource('/items_individuales', 'IndividualItemsController');

Route::resource('/gastos_individuales', 'IndividualExpensesController');

Route::resource('/servicios_individuales', 'IndividualItemsController');

Route::resource('/proveedores', 'ProvidersController');

Route::resource('/unidades', 'UnityController');

Route::resource('/usuario', 'UserController');

Route::get('/usuarioMyExpenses', 'UserController@myExpense');

Route::get('/usuarioBuildingExpenses', 'UserController@buildingExpense');

Route::get('/usuarioRent', 'UserController@rent');

Route::resource('/extension_unidad', 'UnityExtensionController');

Route::get('/dashboard/{id}', 'HomeController@dashboard');

Route::post('/unidades/import', 'UnityController@import');

Route::get('/personal', 'UserController@personal');

Route::get('/inquilino', 'UserController@inquilino');

Route::get('/getIndividualByUnity/{unity_id}', 'HelperController@getIndividualByUnity');

Route::get('/getInquilino/{unity_id}', 'HelperController@getInquilino');

Route::post('/createIndividual', 'HelperController@createIndividual');

Route::get('/gastos_unidad', 'UnityController@expenses');

Route::get('/userLogout', 'HomeController@userLogout');

