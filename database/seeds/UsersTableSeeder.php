<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
            'name' => 'Aldo Verdugo',
            'email' => 'aldo@gmail.com',
            'password' => bcrypt('secret'),
			'rut' => '16250774-5',
			'phone' => '+56979207171',
			'type' => 1,
        ]);
    }
}
