@extends('layouts.app')

@section('css')
	<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')


<div class="form-group row">
	<div class="col-md-8">
		<h1>Mis Gastos Comunes</h1>
	</div>
</div>

<div class="card shadow mb-4">

	<div class="card-body">
	  <div class="table-responsive">
		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			<div class="form-group row">
				<div class="col-md-8">
					<h3>Mis Gastos Comunes</h3>
				</div>
			</div>
		  <thead>
			<tr>
			  <th>Items</th>
			  <th>Descripcion</th>
			  <th>Monto</th>
			  <th>Alicuota %</th>
			  <th>A pagar</th>
			</tr>
		  </thead>

		  <tbody>
			
			<tr>
				<td>1</td>
				<td>Remuneraciones y Honorarios</td>
				<td> 
					@php
						$mount = 0;
						foreach ($remus as $key => $r) {
							$mount =  $mount + $r->amount;
						}
						echo $mount;
					@endphp
				</td>
				<td>{{$user->alicuota}}</td>
			   	<td>{{ $mount / $user->alicuota }} </td>
			</tr>
			
			
			@foreach ($mantens as $key1 => $u1)
			<tr>
				<td>{{$u1->item_id}}</td>
				<td>Mantenciones</td>
				<td>{{$u1->amount}}</td>
				<td>0.7661 %</td>
			   <td>{{$u1->amount}}</td>
				</tr>
			@endforeach
			@foreach ($repairs as $key2 => $u2)
			<tr>
				<td>{{$u2->item_id}}</td>
				<td>Reparaciones</td>
				<td>{{$u2->amount}}</td>
				<td>0.7661 %</td>
			   <td>{{$u2->amount}}</td>
				</tr>
			@endforeach
			@foreach ($basicCs as $key3 => $u3)
			<tr>
				<td>{{$u3->item_id}}</td>
				<td>Consumos Básicos</td>
				<td>{{$u3->amount}}</td>
				<td>0.7661 %</td>
			   <td>{{$u3->amount}}</td>
				</tr>
			@endforeach
			@foreach ($insumos as $key4 => $u4)
			<tr>
				<td>{{$u4->item_id}}</td>
				<td>Insumos</td>
				<td>{{$u4->amount}}</td>
				<td>0.7661 %</td>
			   <td>{{$u4->amount}}</td>
				</tr>
			@endforeach
			@foreach ($otherCs as $key5 => $u5)
			<tr>
				<td>{{$u5->item_id}}</td>
				<td>Otros Gastos</td>
				<td>{{$u->amount}}</td>
				<td>0.7661 %</td>
			   <td>{{$u5->amount}}</td>
				</tr>
			@endforeach
			@foreach ($otherIs as $key6 => $u6)
			<tr>
				<td>{{$u6->item_id}}</td>
				<td>Otros Ingresos</td>
				<td>{{$u6->amount}}</td>
				<td>0.7661 %</td>
			   <td>{{$u6->amount}}</td>
				</tr>
			@endforeach
			<tr>
				<td>Total</td>
				<td></td>
				<td></td>
				<td></td>
			   <td>Pagar</td>
				</tr>

		  </tbody>
		</table>
	  </div>
	</div>
  </div>

  <div class="card shadow mb-4">

	<div class="card-body">
	  <div class="table-responsive">
		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			<div class="form-group row">
				<div class="col-md-8">
					<h3>Mis Gastos Individuales</h3>
				</div>
		  </div>
		  <thead>
			<tr>
			  <th>Items</th>
			  <th>Descripcion</th>
			  <th>Apagar</th>
			</tr>
		  </thead>

		  <tbody>
				<tr>
					<td>1</td>
					<td>Fondo de Reserva</td>
					<td>2.500</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Fondo de indemnnizacion</td>
					<td>0</td>
				</tr>
				<tr>
					<td>Total</td>
					<td></td>
					<td>Pagar</td>
				</tr>

		  </tbody>
		</table>
	  </div>
	</div>
  </div>

  <div class="card shadow mb-4">

	<div class="card-body">
	  <div class="table-responsive">
		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			<div class="form-group row">
				<div class="col-md-8">
					<h3>Saldo Pendiente del Gasto Comun</h3>
				</div>
		  </div>
		  <thead>
			<tr>
				<th>Items</th>
				<th>Descripcion</th>
				<th>Apagar</th>
			</tr>
		  </thead>

		  <tbody>
				<tr>
					<td>1</td>
					<td>Saldo Pendiente</td>
					<td>122.200</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Interes por Atraso</td>
					<td>3.900</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Multa por Atraso</td>
					<td>0</td>
				</tr>
				<tr>
					<td>Total</td>
					<td></td>
					<td>Pagar</td>
				</tr>


		  </tbody>
		</table>
	  </div>
	</div>
  </div>
@endsection

@section('scripts')

	<!-- Page level plugins -->
	<script src="vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="js/demo/datatables-demo.js"></script>
	<script src="js/serializeObject.js"></script>
	<script type="text/javascript">
		$(function () {

		})
	</script>
@endsection