@extends('layouts.app')

@section('css')
	<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')

<div class="col-md-8">
	<h1>Arrendar Lugar</h1>
</div>

<div class="card shadow mb-4">

	<div class="card-body">
	  <div class="table-responsive">
		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			<div class="form-group row">
		  </div>
		  <thead>
			<tr>
			  <th>Nombre</th>
			  <th>Precio</th>
              <th>Garantia</th>
              <th>Valor Multa</th>
			  <th>Lugar</th>
			  <th>Arrendarlo</th>
			</tr>
		  </thead>

		  <tbody>
			@foreach ($items as $key => $u)
				  <tr>
					<td>{{$u->name}}</td>
					<td>${{$u->price}}CLP</td>
                    <td>{{$u->waranty}}</td>
                    <td>${{$u->penalty}}CLP</td>
                    <td>
                        {{$types[$u->type]}}
                        @if ($u->type == 2)
                            <span>/ {{$u->structure->name}}</span>
                        @endif
					</td>
					<td>Pagar</td>
				</tr>
			@endforeach

		  </tbody>
		</table>
	  </div>
	</div>
  </div>
@endsection

@section('scripts')

	<!-- Page level plugins -->
	<script src="vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="js/demo/datatables-demo.js"></script>
	<script src="js/serializeObject.js"></script>
	<script type="text/javascript">
		$(function () {

		})
	</script>
@endsection