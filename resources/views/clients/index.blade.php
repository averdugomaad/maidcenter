@extends('layouts.app')

@section('css')
	<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')

<div class="card shadow mb-4">

	<div class="card-body">
	  <div class="table-responsive">
		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			<div class="form-group row">
				<div class="col-md-4">
					<select class="form-control" name="type">
					  <option value="">Seleccione Fecha de Facturacion</option>
					  <option value="1">01/2020</option>
					  <option value="2">02/2020</option>
					</select>
				</div>
		  </div>
		  <thead>
			<tr>
			  <th>Items</th>
			  <th>Costo</th>
			  <th>Tipo</th>
			</tr>
		  </thead>

		  <tbody>
				  <tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>


		  </tbody>
		</table>
	  </div>
	</div>
  </div>
@endsection

@section('scripts')

	<!-- Page level plugins -->
	<script src="vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="js/demo/datatables-demo.js"></script>
	<script src="js/serializeObject.js"></script>
	<script type="text/javascript">
		$(function () {

		})
	</script>
@endsection