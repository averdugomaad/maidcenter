<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Editar Proovedor</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		</button>
	  </div>
	  <div class="modal-body">
		  <form id="editForm" action="/proveedores" method="post">
			  @csrf
			   <input name="_method" type="hidden" value="PUT">
			  <input type="hidden" name="building_id" value="{{$build->id}}">
			  	<div class="form-group row">
				  	<div class="col-md-12">
					  	<label for="">Nombre</label><br>
					 	<input type="text" class="form-control" name="name" required="" value="">
				  	</div>
			  	</div>
			  	<div class="form-group row">
					<div class="col-md-12">
					  	<label for="">Email</label><br>
					 	<input type="email" class="form-control" name="email" required="" value="">
				  	</div>
			  	</div>
			  	<div class="form-group row">
				  	<div class="col-md-12">
					  	<label for="">Telefono</label><br>
					 	<input type="text" class="form-control" name="phone"  required="" value="">
				  	</div>
			  	</div>
				<div class="form-group row">
				  	<div class="col-md-12">
					  	<label for="">Tipo</label><br>
					 	<select class="form-control" name="type" required="" >
					 		<option value="">Seleccione</option>
							<option value="1">Remuneraciones y Honorarios</option>
							<option value="2">Mantenciones</option>
							<option value="3">Reparaciones</option>
							<option value="4">Consumos Básicos</option>
							<option value="5">Otros Gastos</option>
							<option value="6">Insumos</option>
							<option value="7">Otros Ingresos</option>
					 	</select>
				  	</div>
			  	</div>

	  </div>
	  <div class="modal-footer">
		<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
		<button class="btn btn-primary" type="submit">Guardar</button>
	  </div>
	  </form>
	</div>
  </div>
</div>
