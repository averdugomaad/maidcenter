@extends('layouts.app')

@section('css')
	<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')


	  <!-- Page Heading -->
	  <div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Provedores</h1>

		<a href="#" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#crearModal">
			<i class="fas fa-plus fa-sm text-white-50"></i> Crear Proovedor
		</a>
	  </div>

	  <div class="card shadow mb-4">

		<div class="card-body">
		  <div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			  <thead>
				<tr>
				  <th>Nombre</th>
				  <th>Email</th>
				  <th>Telefono</th>
				  <th>Tipo</th>
				  <th>#</th>
				</tr>
			  </thead>

			  <tbody>
				@foreach ($providers as $key => $u)
				  	<tr>
						<td>{{$u->name}}</td>
						<td>{{$u->email}}</td>
						<td>{{$u->phone}}</td>
						<td>{{$u->type_label}}</td>
						<td data-id="{{$u->id}}">
							<a href="#" class="btn btn-info btn-circle btn-sm editModal" data-toggle="tooltip" data-placement="top" title="Editar">
								<i class="fas fa-pencil-alt"></i>
							</a>
							<form style="display:inline" action="{{ url('/proveedores/'.$u->id) }}" method="post">
								<button  type="submit" class="btn btn-danger btn-circle btn-sm">
    								<i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
    							</button>
							    {!! method_field('delete') !!}
							    {!! csrf_field() !!}
							</form>

						</td>
					</tr>
				@endforeach

			  </tbody>
			</table>
		  </div>
		</div>
	  </div>


@endsection
@section('scripts')

	@include('providers.crearModal')
	@include('providers.editModal')

	<!-- Page level plugins -->
	<script src="vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="js/demo/datatables-demo.js"></script>
	<script src="js/serializeObject.js"></script>
	<script type="text/javascript">
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()

			$('body').on('click', '.editModal', function(){
				var id = $(this).parent().data('id')
				$.get('/proveedores/'+id+'/edit', function(r){
					console.log(r);
					$('#editForm').attr('action', "/proveedores/"+r.id);
					$('#editForm input[name=name]').val(r.name);
					$('#editForm input[name=email]').val(r.email);
					$('#editForm input[name=phone]').val(r.phone);
					$('#editForm select[name=type]').val(r.type);
					$('#editModal').modal()
				})
			})




		})
	</script>
@endsection
