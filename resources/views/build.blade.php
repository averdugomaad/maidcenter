@extends('layouts.building')

@section('content')


	  <!-- Page Heading -->
	  <div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Comunidades</h1>
		<a href="#" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#createBuildingModal"></i> Crear Comunidad</a>
	  </div>

	  <!-- Content Row -->
	  <div class="row">

		<!-- Earnings (Monthly) Card Example -->
		<div v-for="l in lists" class="col-md-4">
			<div  class="card mb-3" style="max-width: 540px;">
			  <div class="row no-gutters">
			    <div class="col-md-4">
					<img src="/storage/{{$l->img->img ?? ''}}">
			    </div>
			    <div class="col-md-8">
			      <div class="card-body">
			        <h5 class="card-title">@{{ l.name }}</h5>
			        <p class="card-text">
			        	<ul class="list-group">
			        		<li style="list-style-type: none;">@{{ l.address }}</li>
							<li style="list-style-type: none;">@{{ l.comuna }}</li>
							<li style="list-style-type: none;">@{{ l.city }}</li>
			        	</ul>
			        </p>
			        <p class="card-text"><a :href="'/dashboard/'+l.id"><small class="">Administrar</small></a></p>
			      </div>
			    </div>
			  </div>
			</div>
		</div>




	  <!-- Content Row -->






@endsection
