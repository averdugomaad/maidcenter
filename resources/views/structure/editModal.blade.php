<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Estructura</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		</button>
	  </div>
	  <form class="" action="" method="post">
			<div class="modal-body">
				@csrf
				<input name="_method" type="hidden" value="PUT">
				<input type="hidden" name="building_id" value="{{$build->id}}">
				<div class="form-group row">
				  	<div class="col-md-6">
					  	<label for="">Nombre</label><br>
					 	<input type="text" class="form-control" name="name" required="" value="">
				  	</div>
					<div class="col-md-6">
					  	<label for="">Tipo</label><br>
						<select class="form-control" name="type" required="">
							<option value="">Seleccione</option>
							@foreach ($types as $key => $value)
								<option value="{{$key}}">{{$value}}</option>
							@endforeach
						</select>
				  	</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
				<button class="btn btn-primary" type="submit">Guardar</button>
			</div>
	  </form>
	</div>
  </div>
</div>
