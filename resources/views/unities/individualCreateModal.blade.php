<div class="modal fade" id="individualCreateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Gastos Individuales</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		</button>
	  </div>
	  <div class="modal-body">
		<nav>
		  <div class="nav nav-tabs" id="nav-tab" role="tablist">
		    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Gastos</a>
		    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Lectura</a>

		  </div>
		</nav>
		<div class="tab-content" id="nav-tabContent">
		  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
			  <form id="individualForm" class="form-inline" action="/extension_unidad"  method="post" style="float:right">
    			  @csrf
    			  <input type="hidden" name="unity_id" value="">
    			  <input type="hidden" name="month" value="{{\Carbon\Carbon::now()->month}}">
    			  <input type="hidden" name="type" value="">
    			  <div class="input-group mb-1 mr-sm-1" style="width: 150px;">
    				  <select  class="form-control" name="individualitem_id"  >
    				  		<option value="">Seleccione</option>
    						@foreach ($indItemsG as $key => $i)
    							<option data-type="{{$i->type}}" value="{{$i->id}}">{{$i->label}}</option>
    						@endforeach
    				  </select>
    			  </div>
    			  <div class="input-group mb-1 mr-sm-1 gastClass" style="width: 150px;">
    				  <input class="form-control" type="number" name="amount" value="" placeholder="Monto">
    			  </div>

    				  <div class="input-group mb-1 mr-sm-1 lectureClass" style="width: 150px;display:none">
    					<input class="form-control" type="number" name="lecture"  placeholder="Lectura Actual">
    				</div>
    				<div class="input-group mb-1 mr-sm-1 lectureClass" style="width: 150px;display:none">
    					<input class="form-control" type="number" name="value"  placeholder="Valor.m3">
    				</div>


    			  <button type="button" class="btn btn-info  mb-1 individualAdd" ><i class="fa fa-plus"></i></button>
    		  </form>
    		  <div class="" style="clear:both"></div>
    		 	<table class="table">
    		 		<thead>
    		 			<tr>
    		 				<th>Item</th>
    						<th>Valor</th>
    						<th>#</th>
    		 			</tr>
    		 		</thead>
    				<tbody id="individualTableContent"></tbody>
    		 	</table>
		  </div>
		  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
			  <form id="individualForm2" class="form-inline" action="/extension_unidad"  method="post" style="float:right">
    			  @csrf
    			  <input type="hidden" name="unity_id" value="">
    			  <input type="hidden" name="month" value="{{\Carbon\Carbon::now()->month}}">
    			  <input type="hidden" name="type" value="">
    			  <div class="input-group mb-1 mr-sm-1" style="width: 150px;">
    				  <select  class="form-control" name="individualitem_id"  >
    				  		<option value="">Seleccione</option>
    						@foreach ($indItemsL as $key => $i)
    							<option data-type="{{$i->type}}" value="{{$i->id}}">{{$i->label}}</option>
    						@endforeach
    				  </select>
    			  </div>


    				  <div class="input-group mb-1 mr-sm-1 " style="width: 150px;">
    					<input class="form-control" type="number" name="lecture"  placeholder="Lectura Actual">
    				</div>
    				<div class="input-group mb-1 mr-sm-1 " style="width: 150px;">
    					<input class="form-control" type="number" name="value"  placeholder="Valor.m3">
    				</div>


    			  <button type="button" class="btn btn-info  mb-1 individualAdd2" ><i class="fa fa-plus"></i></button>
    		  </form>
    		  <div class="" style="clear:both">

    		  </div>
    		 	<table class="table">
    		 		<thead>
    		 			<tr>
    		 				<th>Item</th>
    						<th>Anterior</th>
							<th>Ahora</th>
							<th>Cobro</th>
    						<th>#</th>
    		 			</tr>
    		 		</thead>
    				<tbody id="individualTableContent2"></tbody>
    		 	</table>
		  </div>

		</div>


	  </div>


	</div>
  </div>
</div>
