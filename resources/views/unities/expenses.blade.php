@extends('layouts.app')

@section('css')
	<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')


	  <!-- Page Heading -->
	  <div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Egresos</h1>
	  </div>

	  <div class="card shadow mb-4">

		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered dataTable"  width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Unidad</th>
							<th>Total Gasto Individual</th>
							<th>Total Gasto Comun</th>
							<th>#</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($unities as $key => $u)
							<tr>
								<td>{{$u->label}}</td>
								<td>$15.000</td>
								<td>$13.000</td>
								<td data-id="{{$u->id}}">
									<a href="#" class="btn btn-danger btn-circle btn-sm usersModal">
										<i class="fas fa-users" data-toggle="tooltip" data-placement="top" title="Inquilinos"></i>
									</a>
									<a href="#" class="btn btn-success btn-circle btn-sm individualModal">
										<i class="fas fa-clipboard-list" data-toggle="tooltip" data-placement="top" title="Individual"></i>
									</a>

									<a href="#" class="btn btn-warning btn-circle btn-sm">
										<i class="fas fa-money-check" data-toggle="tooltip" data-placement="top" title="Gasto Comun"></i>
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	  </div>


@endsection
@section('scripts')


	<!-- Page level plugins -->
	<script src="vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="js/demo/datatables-maad.js"></script>
	<script src="js/serializeObject.js"></script>


@endsection
