<div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Inquilinos <span class="deptoLabel"></span></h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		</button>
	  </div>
	  <div class="modal-body">

		  <div class="" style="clear:both">

		  </div>
		 	<table class="table">
		 		<thead>
		 			<tr>
		 				<th>Nombre</th>
						<th>Email</th>
						<th>Rut</th>
						<th>Telefono</th>
						<th>Tipo</th>
		 			</tr>
		 		</thead>
				<tbody id="usersTableContent"></tbody>
		 	</table>
	  </div>


	</div>
  </div>
</div>
