@extends('layouts.app')

@section('css')
	<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')


	  <!-- Page Heading -->
	  <div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Unidades</h1>
		<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm" data-toggle="modal" data-target="#excelModal">
			<i class="fas fa-file-excel fa-sm text-white-50"></i> Cargar Unidades
		</a>
		<a href="#" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#crearModal">
			<i class="fas fa-plus fa-sm text-white-50"></i> Crear Unidad
		</a>
	  </div>

	  <div class="card shadow mb-4">

		<div class="card-body">
		  <div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			  <thead>
				<tr>
				  <th>Etiqueta</th>
				  <th>Tipo</th>
				  <th>Piso</th>
				  <th>Torre</th>
				  <th>Prorateo</th>
				  <th>Rol Asignado</th>
				  <th>#</th>
				</tr>
			  </thead>
			  <tfoot>
				<tr>
				<th>Etiqueta</th>
  				  <th>Tipo</th>
  				  <th>Piso</th>
  				  <th>Torre</th>
  				  <th>Prorateo</th>
  				  <th>Rol Asignado</th>
				  <th>#</th>
				</tr>
			  </tfoot>
			  <tbody>
				@foreach ($unities as $key => $u)
				  	<tr>
						<td>{{$u->label}}</td>
						<td>{{$u->type_label}}</td>
						<td>{{$u->floor}}</td>
						<td>{{$u->tower}}</td>
						<td>{{$u->prorateo}}</td>
						<td>{{$u->assign_rol}}</td>
						<td data-id="{{$u->id}}">

							<a href="#" class="btn btn-success btn-circle btn-sm individualModal">
								<i class="fas fa-clipboard-list" data-toggle="tooltip" data-placement="top" title="Individual"></i>
							</a>


							<a href="#" class="btn btn-info btn-circle btn-sm editModal" data-toggle="tooltip" data-placement="top" title="Editar">
								<i class="fas fa-pencil-alt"></i>
							</a>
							<form style="display:inline" action="{{ url('/unidades/'.$u->id) }}" method="post">
								<button  type="submit" class="btn btn-danger btn-circle btn-sm">
    								<i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
    							</button>
							    {!! method_field('delete') !!}
							    {!! csrf_field() !!}
							</form>

						</td>
					</tr>
				@endforeach

			  </tbody>
			</table>
		  </div>
		</div>
	  </div>


@endsection
@section('scripts')
	@include('unities.excelModal')
	@include('unities.crearModal')
	@include('unities.editarModal')
	@include('unities.extensionCreateModal')
	@include('unities.individualCreateModal')
	@include('unities.usersModal')

	<!-- Page level plugins -->
	<script src="vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="js/demo/datatables-demo.js"></script>
	<script src="js/serializeObject.js"></script>
	<script type="text/javascript">
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()

			$('body').on('click', '.usersModal', function(){
				var id = $(this).parent().data('id')

				$.get('/getInquilino/'+id, function(r){
					$.each(r.data, function(key, val){
						var typeLabel = ['Admin', 'Usuario', 'Propietario','Arrendatario', 'Personal'];
						$('#usersTableContent').append('<tr><td>'+val.name+'</td><td>'+val.email+'</td><td>'+val.rut+'</td><td>'+val.phone+'</td><td>'+typeLabel[val.type]+'</td></tr>')

					})
					$('#usersModal').modal()
				})
			})


			$('body').on('change', '#individualForm select[name=individualitem_id]', function(){
				var type = $(this).find(':selected').attr('data-type');
				$('#individualForm input[name=type]').val(type)
				if (type == 2) {
					$('.lectureClass').show()
					$('.gastClass').hide()
				}else{
					$('.lectureClass').hide()
					$('.gastClass').show()
				}
			})

			$('body').on('click', '.individualModal', function(){
				var id = $(this).parent().data('id')
				$('#individualForm input[name=unity_id]').val(id)
				$('#individualForm2 input[name=unity_id]').val(id)
				$('#individualCreateModal').modal()
				$.get('/getIndividualByUnity/'+id, function(r){
					console.log(r);
					$('#individualTableContent').html(r.data)
					$('#individualTableContent2').html(r.data2)
				})
			})

			$('body').on('click', '.editModal', function(){
				var id = $(this).parent().data('id')
				$.get('/unidades/'+id+'/edit', function(r){
					console.log(r);
					$('#editUnityForm').attr('action', "/unidades/"+r.id);
					$('#editUnityForm input[name=label]').val(r.label);
					$('#editUnityForm select[name=type]').val(r.type);
					$('#editUnityForm input[name=floor]').val(r.floor);
					$('#editUnityForm input[name=tower]').val(r.tower);
					$('#editUnityForm input[name=prorateo]').val(r.prorateo);
					$('#editUnityForm input[name=assign_rol]').val(r.assign_rol);
					$('#editModal').modal()
				})
			})

			$('body').on('click', '.extentionModal', function(){
				var id = $(this).parent().data('id')
				$('#unityIdExtensions').val(id)
				$('#extentionTableContent').html('');
				$.get('/extension_unidad/'+id, function(r){
					$.each(r, function(key, val){
						var typeLabel = ['', 'Estacionamiento', 'Bodega', 'Otro'];
						$('#extentionTableContent').append('<tr><td>'+val.label+'</td><td>'+typeLabel[val.type]+'</td><td>'+val.percent+'</td></tr>')
					})
					$('#extensionCreateModal').modal()
				})
			})
			$('.individualAdd2').on('click', function(){
				var data = $('#individualForm2').serializeObject();
				$.post('/createIndividual', data, function(val){
					$('#individualTableContent2').append(val)
				})
			})

			$('.individualAdd').on('click', function(){
				var data = $('#individualForm').serializeObject();
				$.post('/createIndividual', data, function(val){
					$('#individualTableContent').append(val)
				})
			})

			$('.extentionAdd').on('click', function(){
				var data = $('#extentionForm').serializeObject();
				$.post('/extension_unidad', data, function(val){
					var typeLabel = ['', 'Estacionamiento', 'Bodega', 'Otro'];
					$('#extentionTableContent').append('<tr><td>'+val.label+'</td><td>'+typeLabel[val.type]+'</td><td>'+val.percent+'</td><td data-id="'+val.id+'"><a href="#" class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i></a></td></tr>')
				})
			})
		})
	</script>
@endsection
