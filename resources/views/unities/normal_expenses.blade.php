<div class="card shadow mb-4">
	<div class="card-header">
		<h3>Gasto Comun</h3>
  	</div>
  <div class="card-body">
	<div class="table-responsive">
	  <table class="table table-bordered dataTable" id="" width="100%" cellspacing="0">
		<thead>
		  <tr>
			<th>Fecha</th>
			<th>Egreso</th>
			<th>Proovedor</th>
			<th>Concepto</th>
			<th>Cheque</th>
			<th>Monto</th>
			<th>#</th>
		  </tr>
		</thead>

		<tbody>
			@php
				$insumos = [];
			@endphp
		  @foreach ($insumos as $key => $u)
			  <tr>
				  <td>{{$u->created_at}}</td>
				  <td>{{$u->label}}</td>
				  <td>{{$u->provider_id}}</td>
				  <td>{{$u->concept}}</td>
				  <td>{{$u->bank_draft}}</td>
				 <td>{{$u->amount}}</td>
				  <td data-id="{{$u->id}}">
					  <a href="#" class="btn btn-info btn-circle btn-sm editModal" data-toggle="tooltip" data-placement="top" title="Editar">
						  <i class="fas fa-pencil-alt"></i>
					  </a>
					  <form style="display:inline" action="{{ url('/usuarios/'.$u->id) }}" method="post">
						  <button  type="submit" class="btn btn-danger btn-circle btn-sm">
							  <i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
						  </button>
						  {!! method_field('delete') !!}
						  {!! csrf_field() !!}
					  </form>

				  </td>
			  </tr>
		  @endforeach

		</tbody>
	  </table>
	</div>
  </div>
</div>
