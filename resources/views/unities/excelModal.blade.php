3<div class="modal fade" id="excelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Subir Unidades</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		</button>
	  </div>
	  <div class="modal-body">
		  <form class="" action="/unidades/import" enctype="multipart/form-data" method="post">
			  @csrf
			  <div class="form-group row">
				  <div class="col-md-12">
					  <label for="">Excel</label><br>
					  <input type="file" name="file"  />
				  </div>

			  </div>
	  </div>
	  <div class="modal-footer">
		<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
		<button class="btn btn-primary" type="submit">Guardar</button>
	  </div>
	  </form>
	</div>
  </div>
</div>
