<div class="modal fade" id="extensionCreateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Extensiones</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		</button>
	  </div>
	  <div class="modal-body">
		  <form id="extentionForm" class="form-inline" action="/extension_unidad"  method="post" style="float:right">
			  @csrf
			  <input id="unityIdExtensions" type="hidden" name="unity_id" value="">
			  <div class="input-group mb-1 mr-sm-1" style="width: 150px;">
				  <select class="form-control" name="type"  >
				  		<option value="">Seleccione</option>
						<option value="1">Estacionamiento</option>
						<option value="2">Bodega</option>
						<option value="3">Otro</option>
				  </select>
			  </div>
			  <div class="input-group mb-1 mr-sm-1" style="width: 150px;">
				  <input class="form-control" type="text" name="label" value="" placeholder="Etiqueta">
			  </div>
			  <div class="input-group mb-1 mr-sm-1" style="width: 150px;">
				  <input class="form-control" type="number" name="percent" step="0.0001" value="" placeholder="Porcentaje">
			  </div>
			  <button type="button" class="btn btn-info  mb-1 extentionAdd" ><i class="fa fa-plus"></i></button>
		  </form>
		  <div class="" style="clear:both">

		  </div>
		 	<table class="table">
		 		<thead>
		 			<tr>
		 				<th>Etiqueta</th>
						<th>Tipo</th>
						<th>Porcentaje</th>
						<th>#</th>
		 			</tr>
		 		</thead>
				<tbody id="extentionTableContent"></tbody>
		 	</table>
	  </div>
	</div>
  </div>
</div>
