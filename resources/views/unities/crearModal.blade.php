<div class="modal fade" id="crearModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Crear Unidad</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		</button>
	  </div>
	  <div class="modal-body">
		  <form class="" action="/unidades" method="post">
			  @csrf
			  <input type="hidden" name="building_id" value="{{$build->id}}">
			  	<div class="form-group row">
				  	<div class="col-md-12">
					  	<label for="">Etiqueta</label><br>
					 	<input type="text" class="form-control" name="label" value="" required="">
				  	</div>
			  	</div>
			  	<div class="form-group row">
				  	<div class="col-md-12">
					  	<label for="">Tipo</label><br>
					 	<select class="form-control" name="type" required="">
					 		<option value="">Seleccione</option>
							@foreach ($types as $k => $t)
								<option value="{{$k}}">{{$t}}</option>
							@endforeach
					 	</select>
				  	</div>
			  	</div>
			  	<div class="form-group row">
				  	<div class="col-md-12">
					  	<label for="">Piso</label><br>
					 	<input type="text" class="form-control" name="floor" value="" required="">
				  	</div>
			  	</div>
				<div class="form-group row">
				  	<div class="col-md-12">
					  	<label for="">Torre</label><br>
						<select class="form-control" name="tower" required="">
							<option value="">Seleccione</option>
							@foreach ($towers as $key => $a)
								<option value="{{$a->id}}">{{$a->name}}</option>
							@endforeach
						</select>

				  	</div>
			  	</div>
				<div class="form-group row">
				  	<div class="col-md-12">
					  	<label for="">Prorateo</label><br>
					 	<input type="number" step="0.0001" class="form-control" name="prorateo" required="" value="">
				  	</div>
			  	</div>
				<div class="form-group row">
				  	<div class="col-md-12">
					  	<label for="">Rol Asignado</label><br>
					 	<input type="text"  class="form-control" name="assign_rol" required="" value="">
				  	</div>
			  	</div>

	  </div>
	  <div class="modal-footer">
		<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
		<button class="btn btn-primary" type="submit">Guardar</button>
	  </div>
	  </form>
	</div>
  </div>
</div>
