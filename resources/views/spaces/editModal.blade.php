<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Espacio Comun</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		</button>
	  </div>
	  <form class="" action="" method="post">
			<div class="modal-body">
				@csrf
				<input name="_method" type="hidden" value="PUT">
				<input type="hidden" name="building_id" value="{{$build->id}}">
				<div class="form-group row">
				  	<div class="col-md-6">
					  	<label for="">Nombre</label><br>
					 	<input type="text" class="form-control" name="name" required="" value="">
				  	</div>
					<div class="col-md-6">
						<label for="">Precio</label><br>
						<input type="number" class="form-control" name="price" required="" value="">
					</div>
					<div class="col-md-6">
						<label for="">Garantia</label><br>
						<input type="number" class="form-control" name="waranty" required="" value="">
					</div>
					<div class="col-md-6">
						<label for="">Multa</label><br>
						<input type="number" class="form-control" name="penalty" required="" value="">
					</div>
					<div class="col-md-6">
					  	<label for="">Tipo</label><br>
						<select class="form-control" name="type" required="">
							<option value="">Seleccione</option>
							@foreach ($types as $key => $value)
								<option value="{{$key}}">{{$value}}</option>
							@endforeach
						</select>
				  	</div>
					<div class="col-md-6 estructuraType" style="display:none">
					  	<label for="">Estructura</label><br>
						<select class="form-control" name="buildingitem_id">
							<option value="">Seleccione</option>
							@foreach ($build->structure as $key => $value)
								<option value="{{$value->id}}">{{$value->name}}</option>
							@endforeach
						</select>
				  	</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
				<button class="btn btn-primary" type="submit">Guardar</button>
			</div>
	  </form>
	</div>
  </div>
</div>
