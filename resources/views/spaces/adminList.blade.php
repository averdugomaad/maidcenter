@extends('layouts.app')

@section('css')
	<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')


	  <!-- Page Heading -->
	  <div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Espacios Comunes  {{$build->name}}</h1>

		<a href="#" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#crearModal">
			<i class="fas fa-plus fa-sm text-white-50"></i> Crear Espacio
		</a>
	  </div>

	  <div class="card shadow mb-4">

		<div class="card-body">
		  <div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			  <thead>
				<tr>
				  <th>Nombre</th>
				  <th>Tipo</th>
				  <th>Precio</th>
				  <th>Garantia</th>
				  <th>Multa</th>
				  <th>Tipo</th>
				  <th>#</th>
				</tr>
			  </thead>

			  <tbody>
				@foreach ($items as $key => $u)
				  	<tr>
						<td>{{$u->name}}</td>
						<td>{{$u->type}}</td>
						<td>{{$u->price}}</td>
						<td>{{$u->waranty}}</td>
						<td>{{$u->penalty}}</td>
						<td>
							{{$types[$u->type]}}
							@if ($u->type == 2)
								<span>/ {{$u->structure->name}}</span>
							@endif
						</td>
						<td data-id="{{$u->id}}">
							<a href="#" class="btn btn-info btn-circle btn-sm editModal" data-toggle="tooltip" data-placement="top" title="Editar">
								<i class="fas fa-pencil-alt"></i>
							</a>
							<form style="display:inline" action="{{ url('/espacios/'.$u->id) }}" method="post">
								<button  type="submit" class="btn btn-danger btn-circle btn-sm">
    								<i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
    							</button>
							    {!! method_field('delete') !!}
							    {!! csrf_field() !!}
							</form>

						</td>
					</tr>
				@endforeach

			  </tbody>
			</table>
		  </div>
		</div>
	  </div>


@endsection
@section('scripts')
	@include('spaces.crearModal')
	@include('spaces.editModal')
	<!-- Page level plugins -->
	<script src="vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="js/demo/datatables-demo.js"></script>
	<script src="js/serializeObject.js"></script>
	<script type="text/javascript">
		$(function () {

			$('#crearModal select[name=type]').on('change', function() {
				if (this.value == 2) {
					$('#crearModal .estructuraType').show()
				}else{
					$('#crearModal .estructuraType').hide()
				}
			});

			$('#editModal select[name=type]').on('change', function() {
				if (this.value == 2) {
					$('#editModal .estructuraType').show()
				}else{
					$('#editModal .estructuraType').hide()
				}
			});

			$('body').on('click', '.editModal', function(){
				var id = $(this).parent().data('id');

				$.get('/espacios/'+id, function(r){
					$('#editModal input[name=name]').val(r.name)
					$('#editModal input[name=price]').val(r.price)
					$('#editModal input[name=waranty]').val(r.waranty)
					$('#editModal input[name=penalty]').val(r.penalty)
					$('#editModal select[name=type]').val(r.type)
					if (r.type == 2) {
						$('#editModal .estructuraType').show()
						$('#editModal select[name=buildingitem_id]').val(r.buildingitem_id)
					}else{
						$('#editModal .estructuraType').hide()
					}
					$('#editModal select[name=buildingitem_id]').val(r.buildingitem_id)
					$('#editModal form').attr('action', "/espacios/"+id);
					$('#editModal').modal();
				})
			})
		})
	</script>
@endsection
