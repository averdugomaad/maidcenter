<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/" style="background: white;">
	<div class="sidebar-brand-icon rotate-n-15">

	</div>
	<div class="sidebar-brand-text mx-3"><img src="/img/adech.png" alt="..." style="width: 100%;"></div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
	<a class="nav-link" href="/home">
	  <i class="fas fa-fw fa-tachometer-alt"></i>
	  <span>Dashboard</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
	Interface
  </div>
@if($user->type==3 || $user->type==4)
	<li class="nav-item">
		<a class="nav-link" href="/usuarioMyExpenses">
			<i class="fas fa-fw fa-chart-area"></i>
			<span>Mi Gasto Comun</span>
			</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="/usuarioBuildingExpenses">
			<i class="fas fa-fw fa-chart-area"></i>
			<span>Gastos del Edificio</span>
			</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="/usuarioRent">
			<i class="fas fa-fw fa-chart-area"></i>
			<span>Arrendar</span>
			</a>
	</li>
@endif
@if($user->type==1 || $user->type==2)
  	<li class="nav-item">
	  	<a class="nav-link" href="/estructura">
			<i class="fas fa-fw fa-chart-area"></i>
			<span>Estructura</span>
		</a>
  	</li>
	<li class="nav-item">
	  	<a class="nav-link" href="/espacios">
			<i class="fas fa-fw fa-chart-area"></i>
			<span>Area Comunes</span>
		</a>
  	</li>
  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item">
	<a class="nav-link" href="/unidades">
	  <i class="fas fa-fw fa-chart-area"></i>
	  <span>Unidades</span></a>
  </li>
  <li class="nav-item">
   <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
	 <i class="fas fa-fw fa-cog"></i>
	 <span>Gastos</span>
   </a>
   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
	 <div class="bg-white py-2 collapse-inner rounded">
	   <a class="collapse-item" href="/egresos">Gastos Comunes</a>
	   <a class="collapse-item" href="/servicios_individuales">Individuales</a>
	   	<a class="collapse-item" href="/gastos_unidad">
		   Por Unidad
	    </a>
	 </div>
   </div>
  </li>
  <li class="nav-item">
	<a class="nav-link" href="/fondos">
	  <i class="fas fa-fw fa-chart-area"></i>
	  <span>Fondos</span></a>
  </li>
  <li class="nav-item">
	<a class="nav-link" href="/personal">
	  <i class="fas fa-fw fa-chart-area"></i>
	  <span>Personal</span></a>
  </li>
  <li class="nav-item">
	<a class="nav-link" href="/inquilino">
	  <i class="fas fa-fw fa-chart-area"></i>
	  <span>Inquilino</span></a>
  </li>
  	<li class="nav-item">
  		<a class="nav-link" href="/proveedores">
			<i class="fas fa-fw fa-chart-area"></i>
			<span>Proveedores</span>
		</a>
  	</li>
@endif
  <!-- Nav Item - Pages Collapse Menu -->

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
	<button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
