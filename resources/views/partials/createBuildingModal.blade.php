<div class="modal fade" id="createBuildingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Crear Edificio</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		</button>
	  </div>
	  <form class="" action="/edificios" enctype="multipart/form-data" method="post">
		<div class="modal-body">

			  @csrf
				<div class="form-group row">
				  	<div class="col-md-12">
					  <label for="">Imagen</label><br>
					  <input type="file" name="file"  ref="file"  placeholder="Imagen Perfil" >
					  </div>
				</div>
				<div class="form-group row">
					<div class="col-md-12">
					  <label for="">Nombre</label>
					  <input class="form-control" type="text" name="name" required="" value=""  >
					</div>
					<div class="form-group col-md-6">
						<label for="">Tipo</label>
						<select class="form-control form-control-lg" name="type" >
							<option value="">Seleccione Tipo</option>
							<option value="1">Edificio</option>
							<option value="2">Casa</option>
							<option value="3">Condominio</option>
						</select>
					</div> 
				    <div class="col-md-12">
					  		<label for="">Direccion</label>
							  <input class="form-control" type="text" name="address" required="" value=""  >
				    </div>
					<div class="col-md-6">
						<label for="">Rut</label>
						<input class="form-control" type="text" name="rut" required="" value=""  >
					</div>
					<div class="col-md-6">
						<label for="">Telefono</label>
						<input class="form-control" type="text" name="phone" required="" value=""  >
					</div>
					<div class="col-md-6">
					  	<label for="">Ciudad</label>
						  <input class="form-control" type="text" name="city" required="" value=""  >
					</div>

					<div class="col-md-6">
						<div class="form-group">
						  	<label for="">Comuna</label>
							  <input class="form-control" type="text" name="comuna" required="" value=""  >
						</div>
					</div>
					<div class="col-md-6">
					  	<label for="">Banco</label>
						  <input class="form-control" type="text" name="bank" required="" value=""  >
					</div>
					<div class="col-md-6">
					  	<label for="">Cta de Banco</label>
						  <input class="form-control" type="text" name="account" required="" value=""  >
					</div>
					<div class="col-md-6">
					  	<label for="">Email</label>
						<input class="form-control" type="text" name="trans_email" required="" value=""  >
					</div>
					<div class="col-md-6">
					  	<label for="">Interes</label>
						  <input class="form-control" type="number" step="0.01" name="interest" required="" value=""  >
					</div>
					<div class="col-md-6">
					  	<label for="">Morosidad</label>
						  <input class="form-control" type="number" step="0.01" name="penalty" required="" value=""  >
					</div>
				</div>
		</div>
	  <div class="modal-footer">
		<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
		<button class="btn btn-primary" type="submit">Guardar</button>
	  </div>
	</form>
	</div>
  </div>
</div>
