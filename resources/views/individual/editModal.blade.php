<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Editar Gasto Individual</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		</button>
	  </div>
	  <div class="modal-body">
		  <form id="editForm" action="/proveedores" method="post">
			  @csrf
			   <input name="_method" type="hidden" value="PUT">
			  <input type="hidden" name="building_id" value="{{$build->id}}">
			  	<div class="form-group row">
				  	<div class="col-md-12">
					  	<label for="">Nombre</label><br>
					 	<input type="text" class="form-control" name="label" value="">
				  	</div>
			  	</div>
			  	<div class="form-group row">
				  	<div class="col-md-12">
					  	<label for="">Tipo</label><br>
						<select class="form-control" name="type">
							<option value="">Seleccione Tipo</option>
							<option value="1">Gasto</option>
							<option value="2">Lectura</option>
							
						</select>
				  	</div>
			  	</div>

	  </div>
	  <div class="modal-footer">
		<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
		<button class="btn btn-primary" type="submit">Guardar</button>
	  </div>
	  </form>
	</div>
  </div>
</div>
