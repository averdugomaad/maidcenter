@extends('layouts.app')

@section('css')
	<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')


	  <!-- Page Heading -->
	  <div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Gastos Individuales</h1>


	  </div>

	  <div class="card shadow mb-4">
		  <div class="card-header">

 	 		<form action="/servicios_individuales" method="post" class="form-inline float-right egresoForm">
 	 			{{ csrf_field() }}
 	 			<input type="hidden" name="building_id" value="{{$build->id}}">
 	 		  	<input type="text" name="label" class="form-control mb-2 mr-sm-2" placeholder="Nombre" style="    min-width: 300px;">
				<div class="input-group mb-2 mr-sm-2" style="    min-width: 300px;">
					<select class="form-control" name="type">
						<option value="">Seleccione Tipo</option>
						<option value="1">Gasto</option>
						<option value="2">Lectura</option>
					</select>
				</div>
 	 		  <button type="submit" class="btn btn-primary mb-2">Crear</button>
 	 		</form>
 	   	</div>

		<div class="card-body">
		  <div class="table-responsive">
			<table class="table table-bordered" id="" width="100%" cellspacing="0">
			  <thead>
				<tr>
				  <th>Nombre</th>
				  <th>Tipo</th>
				  <th>#</th>
				</tr>
			  </thead>

			  <tbody>
				@foreach ($indItems as $key => $u)
				  	<tr>
						<td>{{$u->label}}</td>
						<td>{{$u->type_label}}</td>
						<td data-id="{{$u->id}}">
							<a href="#" class="btn btn-info btn-circle btn-sm editModal" data-toggle="tooltip" data-placement="top" title="Editar">
								<i class="fas fa-pencil-alt"></i>
							</a>
							<form style="display:inline" action="{{ url('/servicios_individuales/'.$u->id) }}" method="post">
								<button  type="submit" class="btn btn-danger btn-circle btn-sm">
    								<i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
    							</button>
							    {!! method_field('delete') !!}
							    {!! csrf_field() !!}
							</form>

						</td>
					</tr>
				@endforeach

			  </tbody>
			</table>
		  </div>
		</div>
	  </div>


@endsection
@section('scripts')

	@include('individual.editModal')

	<!-- Page level plugins -->
	<script src="vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="js/demo/datatables-demo.js"></script>
	<script src="js/serializeObject.js"></script>
	<script type="text/javascript">
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()

			$('body').on('click', '.editModal', function(){
				var id = $(this).parent().data('id')
				$.get('/servicios_individuales/'+id+'/edit', function(r){
					$('#editForm').attr('action', "/servicios_individuales/"+r.id);
					$('#editForm input[name=label]').val(r.label);
					$('#editForm select[name=type]').val(r.type);
					$('#editModal').modal()
				})
			})




		})
	</script>
@endsection
