@extends('layouts.app')

@section('css')
	<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')


	  <!-- Page Heading -->
	  <div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Personal {{$build->name}}</h1>

		<a href="#" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#crearModal">
			<i class="fas fa-plus fa-sm text-white-50"></i> Crear Personal
		</a>
	  </div>

	  <div class="card shadow mb-4">

		<div class="card-body">
		  <div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			  <thead>
				<tr>
				  <th>Nombre</th>
				  <th>Rut</th>
				  <th>Cargo</th>
				  <th>Sueldo</th>
				  <th>Leyes Sociales</th>
				  <th>#</th>
				</tr>
			  </thead>

			  <tbody>
				@foreach ($users as $key => $u)
				  	<tr>
						<td>{{$u->name}}</td>
						<td>{{$u->rut}}</td>
						<td>{{$u->personalDetail->position}}</td>
						<td>{{$u->personalDetail->salary}}</td>
						<td>{{$u->personalDetail->previred}}</td>
						<td data-id="{{$u->id}}">
							<a href="#" class="btn btn-info btn-circle btn-sm editModal" data-toggle="tooltip" data-placement="top" title="Editar">
								<i class="fas fa-pencil-alt"></i>
							</a>
							<form style="display:inline" action="{{ url('/usuarios/'.$u->id) }}" method="post">
								<button  type="submit" class="btn btn-danger btn-circle btn-sm">
    								<i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
    							</button>
							    {!! method_field('delete') !!}
							    {!! csrf_field() !!}
							</form>

						</td>
					</tr>
				@endforeach

			  </tbody>
			</table>
		  </div>
		</div>
	  </div>


@endsection
@section('scripts')
	@include('personal.crearModal')

	<!-- Page level plugins -->
	<script src="vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="js/demo/datatables-demo.js"></script>
	<script src="js/serializeObject.js"></script>
	<script type="text/javascript">
		$(function () {

		})
	</script>
@endsection
