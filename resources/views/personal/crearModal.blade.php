<div class="modal fade" id="crearModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Crear Personal</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		</button>
	  </div>
	  <div class="modal-body">
		  <form class="" action="/usuario" method="post">
			  @csrf
			  <input type="hidden" name="type" value="5">
			  <input type="hidden" name="building_id" value="{{$build->id}}">
			  	<div class="form-group row">
				  	<div class="col-md-6">
					  	<label for="">Nombre</label><br>
					 	<input type="text" class="form-control" name="name" required="" value="">
				  	</div>
					<div class="col-md-6">
					  	<label for="">Email</label><br>
					 	<input type="email" class="form-control" name="email" required="" value="">
				  	</div>
			  	</div>

				<div class="form-group row">

				  	<div class="col-md-6">
					  	<label for="">Rut</label><br>
					 	<input type="text" class="form-control" name="rut" required="" value="">
				  	</div>
					<div class="col-md-6">
					  	<label for="">Telefono</label><br>
					 	<input type="text" class="form-control" name="phone" required="" value="">
				  	</div>

			  	</div>

				<div class="form-group row">
					<div class="col-md-6">
					  	<label for="">Password</label><br>
					 	<input type="password" class="form-control" name="password" required="" value="">
				  	</div>
				  	<div class="col-md-6">
					  	<label for="">Confirma Password</label><br>
					 	<input type="password" class="form-control" name="password_confirmation" required="" value="">
				  	</div>
			  	</div>


				<div class="form-group row">
				  	<div class="col-md-6">
					  	<label for="">Cargo</label><br>
					 	<input type="text" class="form-control" name="position" required="" value="">
				  	</div>
					<div class="col-md-6">
					<label for="">Salario</label><br>
					<input type="number" class="form-control" name="salary" required="" value="">
				</div>
			  	</div>

				<div class="form-group row">
				  	<div class="col-md-6">
					  	<label for="">Leyes Sociales</label><br>
					 	<input type="number" class="form-control" name="previred" required="" value="">
				  	</div>
			  	</div>
	  </div>
	  <div class="modal-footer">
		<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
		<button class="btn btn-primary" type="submit">Guardar</button>
	  </div>
	  </form>
	</div>
  </div>
</div>
