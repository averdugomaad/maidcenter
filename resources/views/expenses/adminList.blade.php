@extends('layouts.app')

@section('css')
	<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')


	  <!-- Page Heading -->
	  <div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Egresos</h1>
	  </div>

	  <div class="card shadow mb-4">

		<div class="card-body">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
			    <li class="nav-item">
			      <a class="nav-link active" id="remu-tab" data-toggle="tab" href="#remu" role="tab" aria-controls="remu" aria-selected="true">Remuneraciones y Honorarios</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" id="manten-tab" data-toggle="tab" href="#manten" role="tab" aria-controls="manten" aria-selected="false">Mantenciones</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" id="repair-tab" data-toggle="tab" href="#repair" role="tab" aria-controls="repair" aria-selected="false">Reparaciones</a>
			    </li>
				<li class="nav-item">
			      <a class="nav-link" id="basicC-tab" data-toggle="tab" href="#basicC" role="tab" aria-controls="basicC" aria-selected="false">Consumos Básicos</a>
			    </li>
				<li class="nav-item">
			      <a class="nav-link" id="insumo-tab" data-toggle="tab" href="#insumo" role="tab" aria-controls="insumo" aria-selected="false">Insumos</a>
			    </li>
				<li class="nav-item">
			      <a class="nav-link" id="otherC-tab" data-toggle="tab" href="#otherC" role="tab" aria-controls="otherC" aria-selected="false">Otros Gastos</a>
			    </li>
				<li class="nav-item">
			      <a class="nav-link" id="otherI-tab" data-toggle="tab" href="#otherI" role="tab" aria-controls="otherI" aria-selected="false">Otros Ingresos</a>
			    </li>
			</ul>
			<div class="tab-content" id="myTabContent">
			    <div class="tab-pane fade show active" id="remu" role="tabpanel" aria-labelledby="remu-tab">
					@include('expenses.remuList')
			    </div>
			    <div class="tab-pane fade" id="manten" role="tabpanel" aria-labelledby="manten-tab">
			    	@include('expenses.mantenList')
			    </div>
			    <div class="tab-pane fade" id="repair" role="tabpanel" aria-labelledby="repair-tab">
			    	@include('expenses.repairList')
			    </div>
				<div class="tab-pane fade" id="basicC" role="tabpanel" aria-labelledby="basicC-tab">
					@include('expenses.basicCList')
				</div>
				<div class="tab-pane fade" id="insumo" role="tabpanel" aria-labelledby="insumo-tab">
					@include('expenses.insumoList')
				</div>
				<div class="tab-pane fade" id="otherC" role="tabpanel" aria-labelledby="otherC-tab">
					@include('expenses.otherCList')
				</div>
				<div class="tab-pane fade" id="otherI" role="tabpanel" aria-labelledby="otherI-tab">
					@include('expenses.otherIList')
				</div>
			</div>
		</div>
	  </div>


@endsection
@section('scripts')


	<!-- Page level plugins -->
	<script src="vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="js/demo/datatables-demo.js"></script>
	<script src="js/serializeObject.js"></script>


@endsection
