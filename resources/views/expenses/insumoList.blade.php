<div class="card shadow mb-4">
	<div class="card-header">
		<form action="/egresos" method="post" class="form-inline float-right egresoForm">
			{{ csrf_field() }}
			<input type="hidden" name="item_id" value="5">
			<input type="hidden" name="building_id" value="{{$build->id}}">
		  	<input type="text" name="label" class="form-control mb-2 mr-sm-2" placeholder="Egreso">
			<div class="input-group mb-2 mr-sm-2">
				<select class="form-control" name="provider_id">
					<option value="">Seleccione Proovedor</option>
					@foreach ($providers as $key => $p)
						@if ($p->type == 5)
							<option value="{{$p->id}}">{{$p->name}}</option>
						@endif
					@endforeach
				</select>
			</div>
			<div class="input-group mb-2 mr-sm-2">
				<input type="text" name="concept" class="form-control mb-2 mr-sm-2" placeholder="Concepto">
			</div>
			<div class="input-group mb-2 mr-sm-2">
				<input type="text" name="month" class="form-control mb-2 mr-sm-2" required="" placeholder="Mes">
			</div>
			<div class="input-group mb-2 mr-sm-2">
				<input type="text" name="bank_draft" class="form-control mb-2 mr-sm-2" placeholder="Cheque">
			</div>
			<div class="input-group mb-2 mr-sm-2">
				<input type="text" name="amount" class="form-control mb-2 mr-sm-2" placeholder="Monto">
			</div>
		  <button type="submit" class="btn btn-primary mb-2">Crear</button>
		</form>
  	</div>
  <div class="card-body">
	<div class="table-responsive">
	  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		<thead>
		  <tr>
			<th>Fecha</th>
			<th>Egreso</th>
			<th>Proovedor</th>
			<th>Concepto</th>
			<th>Cheque</th>
			<th>Monto</th>
			<th>#</th>
		  </tr>
		</thead>

		<tbody>
		  @foreach ($insumos as $key => $u)
			  <tr>
				  <td>{{$u->created_at}}</td>
				  <td>{{$u->label}}</td>
				  <td>{{$u->provider_id}}</td>
				  <td>{{$u->concept}}</td>
				  <td>{{$u->bank_draft}}</td>
				 <td>{{$u->amount}}</td>
				  <td data-id="{{$u->id}}">
					  <a href="#" class="btn btn-info btn-circle btn-sm editModal" data-toggle="tooltip" data-placement="top" title="Editar">
						  <i class="fas fa-pencil-alt"></i>
					  </a>
					  <form style="display:inline" action="{{ url('/egresos/'.$u->id) }}" method="post">
						  <button  type="submit" class="btn btn-danger btn-circle btn-sm">
							  <i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
						  </button>
						  {!! method_field('delete') !!}
						  {!! csrf_field() !!}
					  </form>

				  </td>
			  </tr>
		  @endforeach

		</tbody>
	  </table>
	</div>
  </div>
</div>
