@extends('layouts.app')

@section('css')
	<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')


	  <!-- Page Heading -->
	  	<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">
				Inquilinos {{$build->name}}
			</h1>
			<div class="">
				<a href="#" id="propT" class="d-sm-inline-block btn btn-sm btn-success shadow-sm" >
					Propietarios
				</a>
				<a href="#" id="propR" class="d-sm-inline-block btn btn-sm btn-info shadow-sm">
					Residentes
				</a>
			</div>
			<a href="#" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#crearModal">
				Crear Inquilino
			</a>
	  	</div>

	  <div id="propCard" class="card shadow mb-4">
		<div class="card-header">
			<h4>Propietarios</h4>
		</div>
		<div class="card-body">
		  <div class="table-responsive">
			<table class="table table-bordered dataTable" width="100%" cellspacing="0">
			  <thead>
				<tr>
					<th>Nombre</th>
  				  	<th>Email</th>
  				  	<th>Rut</th>
  				  	<th>Telefono</th>
  				  	<th>Unidad</th>
					<th>Prorateo</th>
				  	<th>#</th>
				</tr>
			  </thead>

			  <tbody>
				@foreach ($propietarios as $key => $u)
				  	<tr>
						<td>{{$u->name}}</td>
						<td>{{$u->email}}</td>
						<td>{{$u->rut}}</td>
						<td>{{$u->phone}}</td>
						<td>
							@foreach ($u->residentunity as $key => $d)
								<span>{{$d->unity->label}}, </span>
							@endforeach
						</td>
						<td>
							<?php
								$pro = 0;
								foreach ($u->residentunity as $key => $d) {
									$pro = (float)$d->unity->prorateo + (float)$pro;
								}
							?>
							<span>{{$pro}}</span>
						</td>
						<td  data-id="{{$u->id}}">
							<a href="#" class="btn btn-primary btn-circle btn-sm extentionModal">
								<i class="fas fa-warehouse" data-toggle="tooltip" data-placement="top" title="Asociacion"></i>
							</a>

							<a href="#" class="btn btn-info btn-circle btn-sm editModal" data-toggle="tooltip" data-placement="top" title="Editar">
								<i class="fas fa-pencil-alt"></i>
							</a>
							<form style="display:inline" action="{{ url('/usuario/'.$u->id) }}" method="post">
								<button  type="submit" class="btn btn-danger btn-circle btn-sm">
    								<i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
    							</button>
							    {!! method_field('delete') !!}
							    {!! csrf_field() !!}
							</form>

						</td>
					</tr>
				@endforeach

			  </tbody>
			</table>
		  </div>
		</div>
	  </div>
	  <div id="resdCard" class="card shadow mb-4" style="display:none">
		<div class="card-header">
			<h4>Residentes</h4>
		</div>
		<div class="card-body">
		  <div class="table-responsive">
			<table class="table table-bordered dataTable" width="100%" cellspacing="0">
			  <thead>
				<tr>
					<th>Nombre</th>
  				  	<th>Email</th>
  				  	<th>Rut</th>
  				  	<th>Telefono</th>
  				  	<th>Unidad</th>
				  	<th>#</th>
				</tr>
			  </thead>

			  <tbody>
				@foreach ($residentes as $key => $r)
				  	<tr>
						<td>{{$r->name}}</td>
						<td>{{$r->email}}</td>
						<td>{{$r->rut}}</td>
						<td>{{$r->phone}}</td>
						<td>
							@foreach ($r->residentunity as $key => $da)
								<span>{{$da->unity->label}}, </span>
							@endforeach
						</td>
						<td  data-id="{{$r->id}}">
							<a href="#" class="btn btn-primary btn-circle btn-sm extentionModal">
								<i class="fas fa-warehouse" data-toggle="tooltip" data-placement="top" title="Asociacion"></i>
							</a>

							<a href="#" class="btn btn-info btn-circle btn-sm editModal" data-toggle="tooltip" data-placement="top" title="Editar">
								<i class="fas fa-pencil-alt"></i>
							</a>
							<form style="display:inline" action="{{ url('/usuario/'.$r->id) }}" method="post">
								<button  type="submit" class="btn btn-danger btn-circle btn-sm">
    								<i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
    							</button>
							    {!! method_field('delete') !!}
							    {!! csrf_field() !!}
							</form>

						</td>
					</tr>
				@endforeach

			  </tbody>
			</table>
		  </div>
		</div>
	  </div>


@endsection
@section('scripts')
	@include('inquilino.crearModal')
	@include('inquilino.unitiesModal')
	@include('inquilino.editModal')

	<!-- Page level plugins -->
	<script src="vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="js/demo/datatables-maad.js"></script>
	<script src="js/serializeObject.js"></script>
	<script type="text/javascript">

		$(function () {

			$('#propT').on('click', function(){
				$('#propCard').show()
				$('#resdCard').hide()
			})
			$('#propR').on('click', function(){
				$('#propCard').hide()
				$('#resdCard').show()
			})

			var user_id;
			$('body').on('click', '.extentionModal',function(){
				$('.form-check-input').prop('checked', false);
				user_id = $(this).parent().data('id');

				$.get('/asociaciones/'+user_id, function(r){
					$.each(r, function (key, val) {
						$('#checkB'+val.unity_id).prop('checked', true);
				    });
				})

				$('#userIdUnity').val(user_id);
				$('#unitiesModal').modal();
			})

			$('body').on('click', '.editModal', function(){
				var id = $(this).parent().data('id');
				$.get('/usuario/'+id, function(r){
					$('#editModal select[name=type]').val(r.type)
					$('#editModal input[name=name]').val(r.name)
					$('#editModal input[name=rut]').val(r.rut)
					$('#editModal input[name=email]').val(r.email)
					$('#editModal input[name=phone]').val(r.phone)
					$('#editModal form').attr('action', "/usuario/"+id);
					$('#editModal').modal();
				})
			})
		})
	</script>
@endsection
