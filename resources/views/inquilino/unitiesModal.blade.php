<div class="modal fade" id="unitiesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Asociar Unidades</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		</button>
	  </div>
	  	<form class="" action="/asociaciones" method="post">
			@csrf
			<input id="userIdUnity" type="hidden" name="user_id" value="">
			<input type="hidden" name="building_id" value="{{$build->id}}">
			<div class="modal-body">
			  <table class="table dataTable">
			  	<thead>
					<tr>
						<th>Etiqueta</th>
						<th>Tipo</th>
						<th>#</th>
					</tr>
			  	</thead>
				<tbody>
					@foreach ($build->unities as $k => $u)
						<tr>
							<td>{{$u->label}}</td>
							<td>{{$u->type_label}}</td>
							<td>
								<input id="checkB{{$u->id}}" class="form-check-input" name="unities[]" type="checkbox" value="{{$u->id}}">
							</td>
						</tr>
					@endforeach
				</tbody>
			  </table>
			</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
				<button class="btn btn-primary" type="submit">Guardar</button>
			</div>
		  </form>
	</div>
  </div>
</div>
