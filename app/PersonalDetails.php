<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalDetails extends Model
{
    protected $fillable = ['salary', 'previred', 'position', 'user_id'];
}
