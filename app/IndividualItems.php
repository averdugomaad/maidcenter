<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndividualItems extends Model
{
    protected $fillable = ['building_id', 'type', 'label'];

	public $arrayTypes = [1=>'Gasto', 2=>'Lectura'];

	public function getTypeLabelAttribute()
	{
		return $this->arrayTypes[$this->type];
	}
}
