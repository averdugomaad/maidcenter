<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Space extends Model
{
    protected $fillable = [
		'name',
		'price',
		'waranty',
		'penalty',
		'type',
		'building_id',
		'buildingitem_id'
	];

	const TYPE = [
		1 => 'Normal',
		2 => 'Asociado'
	];

	public function structure()
	{
		return $this->belongsTo('App\BuildingItems', 'buildingitem_id');
	}
}
