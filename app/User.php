<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
		'rut', 'phone', 'type',
		'building_id', 'unity_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

	const TYPES = [
		0=>'Super Admin',
		1=>'Admin',
		2=>'Usuario',
		3=>'Propietario',
		4=>'Residente',
		5=>'Personal',
		6=>'Comite'
	];
	//comite puede ver lo que pasa en el edificio solo para ver
	//

	public function personalDetail()
	{
		return $this->hasOne('App\PersonalDetails');
	}

	public function residentunity()
	{
		return $this->hasMany('App\ResidentUnity');
	}

	public function getTypeLabelAttribute()
	{
		return Self::TYPES[$this->type];
    }
    
    public function getAlicuotaAttribute() {
        $pro = 0;
        foreach ($this->residentunity as $key => $d) {
            $pro = (float)$d->unity->prorateo + (float)$pro;
        }
        return $pro;
    }

}
