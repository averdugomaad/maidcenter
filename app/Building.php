<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $fillable = [
		'name', 'address', 'city',
		'comuna', 'type', 'status',
		'img', 'rut', 'phone','bank', '
		account', 'trans_email',
		'interest', 'penalty'
	];

	public function unities()
	{
		return $this->hasMany('App\Unity');
	}

	public function structure()
	{
		return $this->hasMany('App\BuildingItems');
	}
}
