<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResidentUnity extends Model
{
    protected $fillable = [
		'unity_id',
		'user_id',
		'building_id'
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
	public function unity()
	{
		return $this->belongsTo('App\Unity');
	}

}
