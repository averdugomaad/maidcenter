<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndividualExpenses extends Model
{

	protected $fillable = ['unity_id', 'month', 'individualitem_id', 'amount'];

	public function item()
	{
		return $this->belongsTo('App\IndividualItems', 'individualitem_id');
	}

}
