<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class IndividualServices extends Model
{
	protected $fillable = ['unity_id', 'month', 'individualitem_id', 'lecture', 'value'];

	public function item()
	{
		return $this->belongsTo('App\IndividualItems', 'individualitem_id');
	}

	public function getBeforeAttribute()
	{
		$p = Carbon::parse($this->created_at);
		$a = $p->subMonth();
		$m = $a->month < 10 ? '0'.$a->month :$a->month;

		$s = Self::where('unity_id', $this->unity_id)->where('individualitem_id', $this->individualitem_id)->where('created_at','like',$a->year.'-'.$m. '-%')->first();

		return $s->lecture;
	}
}
