<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuildingItems extends Model
{
	protected $fillable = ['name', 'type', 'building_id'];

	const TYPES = [
		1 => 'Torre',
		2 =>'Quincho'
	];

	public function build()
	{
		return $this->belongsTo('App\Building');
	}







}
