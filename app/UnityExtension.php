<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnityExtension extends Model
{
    protected $fillable = ['label', 'type', 'percent', 'unity_id'];

	public $arrayTypes = [1=>'Estacionamiento', 2 => 'Bodega', 3 => 'Otros'];

	public function getTypeLabelAttribute()
    {
        return $this->arrayTypes[$this->type];
    }
}
