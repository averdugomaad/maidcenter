<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = ['label', 'provider_id', 'concept', 'bank_draft', 'amount', 'item_id', 'building_id', 'month', 'img'];
}
