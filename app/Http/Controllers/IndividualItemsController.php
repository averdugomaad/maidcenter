<?php

namespace App\Http\Controllers;

use App\{IndividualItems, Building};
use Illuminate\Http\Request;
use Auth;

class IndividualItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $build =  Building::find(\Session::get('building_id'));
		$indItems = IndividualItems::where('building_id', $build->id)->get();

		return view('individual.adminList', ['indItems'=>$indItems,'build'=>$build,'user'=>$user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $user = Auth::user();
        IndividualItems::create($request->all());
		return back()->with('status', 'Gasto Individual Creado!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IndividualItems  $individualItems
     * @return \Illuminate\Http\Response
     */
    public function show(IndividualItems $individualItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IndividualItems  $individualItems
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json(IndividualItems::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IndividualItems  $individualItems
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		IndividualItems::find($id)->update($request->all());
		return back()->with('status', 'Gasto Individual Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IndividualItems  $individualItems
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        IndividualItems::find($id)->delete();
		return back()->with('status', 'Gasto Individual Borrado!');
    }
}
