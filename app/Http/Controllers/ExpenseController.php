<?php

namespace App\Http\Controllers;

use App\{Expense, Building, User, Providers};
use Illuminate\Http\Request;
use Auth;

class ExpenseController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
		$build =  Building::find(\Session::get('building_id'));
		$personal = User::where('type', 5)->where('building_id', $build->id)->get();
		$remus = Expense::where('building_id', $build->id)->where('item_id', 1)->get();
		$mantens = Expense::where('building_id', $build->id)->where('item_id', 2)->get();
		$repairs = Expense::where('building_id', $build->id)->where('item_id', 3)->get();
		$basicCs = Expense::where('building_id', $build->id)->where('item_id', 4)->get();
		$insumos = Expense::where('building_id', $build->id)->where('item_id', 5)->get();
		$otherCs = Expense::where('building_id', $build->id)->where('item_id', 6)->get();
		$otherIs = Expense::where('building_id', $build->id)->where('item_id', 7)->get();

		$providers = Providers::where('building_id', $build->id)->get();

        return view('expenses.adminList', ['providers'=>$providers,'mantens'=>$mantens, 'repairs'=>$repairs, 'basicCs'=>$basicCs, 'insumos'=>$insumos, 'otherCs'=>$otherCs, 'otherIs'=>$otherIs, 'remus'=>$remus,'build'=>$build, 'personal'=>$personal, 'user'=>$user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Expense::create($request->all());
		return back()->with('status', 'Gasto Creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function edit(Expense $expense)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Expense $expense)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Expense::find($id)->delete();
		return back()->with('status', 'Borrado!');
    }
    
}
