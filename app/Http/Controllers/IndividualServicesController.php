<?php

namespace App\Http\Controllers;

use App\IndividualServices;
use Illuminate\Http\Request;

class IndividualServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IndividualServices  $individualServices
     * @return \Illuminate\Http\Response
     */
    public function show(IndividualServices $individualServices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IndividualServices  $individualServices
     * @return \Illuminate\Http\Response
     */
    public function edit(IndividualServices $individualServices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IndividualServices  $individualServices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IndividualServices $individualServices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IndividualServices  $individualServices
     * @return \Illuminate\Http\Response
     */
    public function destroy(IndividualServices $individualServices)
    {
        //
    }
}
