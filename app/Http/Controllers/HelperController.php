<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{IndividualExpenses, IndividualServices, User};
use Carbon\Carbon;

class HelperController extends Controller
{

	public function getInquilino($unity_id)
	{
		return response()->json(User::where('unity_id', $unity_id)->get());
	}

	public function createIndividual(Request $request)
	{
		$html = "";
		if ($request->type == 1) {
			$is = IndividualExpenses::create($request->all());
			$html = sprintf('
				<tr>
					<td>%s</td>
					<td>%s</td>
					<td data-id="%s">
						<button  type="submit" class="btn btn-danger btn-circle btn-sm">
							<i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
						</button>
					</td>
				',$is->item->label, $is->amount, $is->id
			);
		}else {
			$is = IndividualServices::create($request->all());
			$html = sprintf('
				<tr>
					<td>%s</td>
					<td>%s</td>
					<td data-id="%s">
						<button  type="submit" class="btn btn-danger btn-circle btn-sm">
							<i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
						</button>
					</td>
				',$is->item->label, $is->lecture * $is->value, $is->id
			);

		}

		return $html;

	}

    public function getIndividualByUnity($unity_id)
	{
		$now = Carbon::now();

		$expenses = IndividualExpenses::where('unity_id', $unity_id)->where('month', $now->month)->get();
		$services = IndividualServices::where('unity_id', $unity_id)->where('month', $now->month)->get();
		$data = $data2 = "";
		foreach ($expenses as $key => $e) {

			$data .= sprintf("
				<tr>
					<td>%s</td>
					<td>%s</td>
					<td data-id='%s'>
						<button  type='submit' class='btn btn-danger btn-circle btn-sm'>
							<i class='fas fa-trash' data-toggle='tooltip' data-placement='top' title='Borrar'></i>
						</button>
					</td>
				",$e->item->label, $e->amount, $e->id
			);
		}
		

		foreach ($services as $key => $e) {

			$data2 .= sprintf('
				<tr>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td data-id="%s">
						<button  type="submit" class="btn btn-danger btn-circle btn-sm">
							<i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Borrar"></i>
						</button>
					</td>
				',$e->item->label, 0/*$e->before*/,$e->lecture, $e->lecture * $e->value, $e->id
			);
		}

		return response()->json(['data'=>$data, 'data2'=>$data2]);
	}
}
