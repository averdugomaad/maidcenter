<?php

namespace App\Http\Controllers;

use App\{Building, BuildingItems};
use Illuminate\Http\Request;
use Auth;

class BuildingItemsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $build =  Building::find(\Session::get('building_id'));
		$items = BuildingItems::where('building_id', $build->id)->get();
		$types = BuildingItems::TYPES;
		return view('structure.adminList', ['types'=>$types, 'items'=>$items,'build'=>$build, 'user'=>$user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        BuildingItems::create($request->all());
		return back()->with('status', 'Creado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BuildingItems  $buildingItems
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return BuildingItems::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BuildingItems  $buildingItems
     * @return \Illuminate\Http\Response
     */
    public function edit(BuildingItems $buildingItems)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BuildingItems  $buildingItems
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$b = BuildingItems::find($id);
		$b->update( $request->all() );
		return back()->with('status', 'Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BuildingItems  $buildingItems
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BuildingItems::find($id)->delete();
		return back()->with('status', 'Borrado!');
    }
}
