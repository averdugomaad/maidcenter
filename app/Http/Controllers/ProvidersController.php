<?php

namespace App\Http\Controllers;

use App\{Providers, Building};
use Illuminate\Http\Request;
use Auth;

class ProvidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = Auth::user();      
      $build =  Building::find(\Session::get('building_id'));
      $providers = Providers::where('building_id', $build->id)->get();
      return view('providers.adminList', ['build'=>$build, 'providers'=>$providers, 'user'=>$user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Providers::create($request->all());
		return back()->with('status', 'Proovedor Creado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Providers  $providers
     * @return \Illuminate\Http\Response
     */
    public function show(Providers $providers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Providers  $providers
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return response()->json(Providers::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Providers  $providers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		Providers::find($id)->update($request->all());
		return back()->with('status', 'Proovedor Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Providers  $providers
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Providers::find($id)->delete();
		return back()->with('status', 'Proovedor Borrado!');
    }
}
