<?php

namespace App\Http\Controllers;

use App\{Space, Building, BuildingItems};
use Illuminate\Http\Request;
use Auth;

class SpaceController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $user = Auth::user();
		$build =  Building::find(\Session::get('building_id'));
		$types = Space::TYPE;
		$towers = BuildingItems::where('type',1)->where('building_id', $build->id)->get();
		$spaces = Space::where('building_id', $build->id)->get();

		return view('spaces.adminList', ['types'=>$types, 'towers'=>$towers,'build'=>$build, 'items'=>$spaces,'user'=>$user]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		Space::create($request->all());
		return back()->with('status', 'Creado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Space  $space
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Space::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Space  $space
     * @return \Illuminate\Http\Response
     */
    public function edit(Space $space)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Space  $space
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$b = Space::find($id);
		$b->update( $request->all() );
		return back()->with('status', 'Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Space  $space
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		Space::find($id)->delete();
		return back()->with('status', 'Borrado!');
    }
}
