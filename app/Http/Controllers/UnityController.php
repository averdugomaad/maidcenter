<?php

namespace App\Http\Controllers;

use App\{Unity, Building, IndividualItems, BuildingItems};
use Illuminate\Http\Request;
use App\Imports\UnitiesImport;
use Auth;

class UnityController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
		$build =  Building::find(\Session::get('building_id'));
		$unities = Unity::where('building_id', $build->id)->get();
		$indItemsG = IndividualItems::where('building_id', $build->id)->where('type',1)->get();
		$indItemsL = IndividualItems::where('building_id', $build->id)->where('type',2)->get();
		$types = Unity::TYPES;
		$towers = BuildingItems::where('type',1)->where('building_id', $build->id)->get();
        return view('unities.adminList', ['types'=>$types, 'towers'=>$towers, 'indItemsG'=>$indItemsG,'indItemsL'=>$indItemsL,'build'=>$build, 'unities'=>$unities,'user'=>$user]);
    }


	public function import(Request $request)
    {
        Excel::import(new UnitiesImport, $request->file('file'));

        return back()->with('success', 'All good!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Unity::create( $request->all() );
		return back()->with('status', 'Unidad Creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unity  $unity
     * @return \Illuminate\Http\Response
     */
    public function show(Unity $unity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unity  $unity
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Unity::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unity  $unity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Unity::find($id)->update($request->all());
		return back()->with('status', 'Unidad Actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unity  $unity
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		Unity::find($id)->delete();
		return back()->with('status', 'Unidad Borrada');
    }

	public function expenses()
	{
        $user = Auth::user();
		$build =  Building::find(\Session::get('building_id'));
		$unities = Unity::where('building_id', $build->id)->get();

		return view('unities.expenses', ['unities'=>$unities, 'build'=>$build, 'user'=>$user]);

	}
}
