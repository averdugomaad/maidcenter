<?php

namespace App\Http\Controllers;

use App\ResidentUnity;
use Illuminate\Http\Request;

class ResidentUnityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		ResidentUnity::where('user_id', $request->user_id)->delete();

		if (!$request->unities)
			return back()->with('status', 'Guardado!');
		

		foreach ($request->unities as $k => $v) {

			ResidentUnity::create([
				'unity_id' => (int)$v,
				'user_id' => $request->user_id,
				'building_id' => $request->building_id,
			]);
		}

		return back()->with('status', 'Guardado!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ResidentUnity  $residentUnity
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ResidentUnity::where('user_id', $id)->get();


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ResidentUnity  $residentUnity
     * @return \Illuminate\Http\Response
     */
    public function edit(ResidentUnity $residentUnity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ResidentUnity  $residentUnity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResidentUnity $residentUnity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ResidentUnity  $residentUnity
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResidentUnity $residentUnity)
    {
        //
    }
}
