<?php

namespace App\Http\Controllers;

use App\{IndividualExpenses, Unity};
use Illuminate\Http\Request;
use App\{Building, BuildingItems};

class IndividualExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$build =  Building::find(\Session::get('building_id'));
		$items = Unity::where('building_id', $build->id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IndividualExpenses  $individualExpenses
     * @return \Illuminate\Http\Response
     */
    public function show(IndividualExpenses $individualExpenses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IndividualExpenses  $individualExpenses
     * @return \Illuminate\Http\Response
     */
    public function edit(IndividualExpenses $individualExpenses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IndividualExpenses  $individualExpenses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IndividualExpenses $individualExpenses)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IndividualExpenses  $individualExpenses
     * @return \Illuminate\Http\Response
     */
    public function destroy(IndividualExpenses $individualExpenses)
    {
        //
    }
}
