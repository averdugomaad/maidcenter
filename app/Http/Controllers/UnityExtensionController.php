<?php

namespace App\Http\Controllers;

use App\UnityExtension;
use Illuminate\Http\Request;

class UnityExtensionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return UnityExtension::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UnityExtension  $unityExtension
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$u = UnityExtension::where('unity_id', $id)->get();
        return response()->json($u);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UnityExtension  $unityExtension
     * @return \Illuminate\Http\Response
     */
    public function edit(UnityExtension $unityExtension)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UnityExtension  $unityExtension
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UnityExtension $unityExtension)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UnityExtension  $unityExtension
     * @return \Illuminate\Http\Response
     */
    public function destroy(UnityExtension $unityExtension)
    {
        //
    }
}
