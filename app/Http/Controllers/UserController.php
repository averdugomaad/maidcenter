<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Building;
use Auth;
use App\Unity;
use App\Expense;
use App\{Space, BuildingItems};

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('clients.index', ['user'=>$user]);
    }

	public function personal()
	{
        $user = Auth::user();
		$build =  Building::find(\Session::get('building_id'));
		$users = User::where('type', 1)->where('building_id', $build->id)->get();
		return view('personal.adminList', ['build'=>$build, 'users'=>$users, 'user'=>$user]);
	}

	public function inquilino()
	{
        $user = Auth::user();
		$build =  Building::find(\Session::get('building_id'));
		$propietarios = User::where('type', 3)
					->where('building_id', $build->id)
					->get();
		$residentes = User::where('type', 4)
					->where('building_id', $build->id)
					->get();

		$unities = Unity::where('building_id', $build->id)->get();


		return view('inquilino.adminList', [
			'unities'=>$unities, 'build'=>$build, 'propietarios'=>$propietarios,
			'residentes' => $residentes, 'user'=>$user
		]);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create($request->all());
		$user->password = bcrypt($request->password);
		$user->save();

		if ($user->type == 5) {
			$details = PersonalDetails::create([
				'salary'	=> $request->salary,
				'afp'		=> $request->afp,
				'health'	=> $request->health,
				'position'	=> $request->position,
				'user_id'	=> $user->id
			]);

			// code...
		}

		return back()->with('status', 'Usuario Creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json( User::find($id) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$b = User::find($id);
		$b->update( $request->all() );
		return back()->with('status', 'Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

		User::find($id)->delete();

        return back()->with('status', 'Usuario Eliminado');
    }
    public function myExpense()
    {
        $user = Auth::user();
        
        $build =  Building::find($user->building_id);
        
        $remus = Expense::where('building_id', $build->id)->where('item_id', 1)->get();
        //dd($remus);
		$mantens = Expense::where('building_id', $build->id)->where('item_id', 2)->get();
		$repairs = Expense::where('building_id', $build->id)->where('item_id', 3)->get();
		$basicCs = Expense::where('building_id', $build->id)->where('item_id', 4)->get();
		$insumos = Expense::where('building_id', $build->id)->where('item_id', 5)->get();
		$otherCs = Expense::where('building_id', $build->id)->where('item_id', 6)->get();
		$otherIs = Expense::where('building_id', $build->id)->where('item_id', 7)->get();
        

        return view('clients.myExpense', ['mantens'=>$mantens, 'repairs'=>$repairs, 'basicCs'=>$basicCs, 'insumos'=>$insumos, 'otherCs'=>$otherCs, 'otherIs'=>$otherIs, 'remus'=>$remus,'build'=>$build, 'user'=>$user]);
    }

    public function buildingExpense()
    {
        $user = Auth::user();
        $build =  Building::find(\Session::get('building_id'));
		$remus = Expense::where('building_id', $build->id)->where('item_id', 1)->get();
		$mantens = Expense::where('building_id', $build->id)->where('item_id', 2)->get();
		$repairs = Expense::where('building_id', $build->id)->where('item_id', 3)->get();
		$basicCs = Expense::where('building_id', $build->id)->where('item_id', 4)->get();
		$insumos = Expense::where('building_id', $build->id)->where('item_id', 5)->get();
		$otherCs = Expense::where('building_id', $build->id)->where('item_id', 6)->get();
        $otherIs = Expense::where('building_id', $build->id)->where('item_id', 7)->get();
        
        return view('clients.buildingExpense', ['mantens'=>$mantens, 'repairs'=>$repairs, 'basicCs'=>$basicCs, 'insumos'=>$insumos, 'otherCs'=>$otherCs, 'otherIs'=>$otherIs, 'remus'=>$remus,'build'=>$build, 'user'=>$user]);
    }
    public function rent()
    {
        $user = Auth::user();
        $build =  Building::find(\Session::get('building_id'));
		$types = Space::TYPE;
        $spaces = Space::where('building_id', $user->building_id)->get();
        return view('clients.rent', ['types'=>$types, 'items'=>$spaces,'user'=>$user]);
    }
    public function contact()
    {
        $user = Auth::contact();
        return view('clients.contact', ['user'=>$user]);
    }

}
