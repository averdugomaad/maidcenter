<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Building;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard($id)
    {

        $user = Auth::user();
		\Session::put('building_id',$id);
		$build = Building::find($id);
        return view('dashboard', ['build'=>$build,'user'=>$user]);
    }
	public function index()
    {
        $user = Auth::user();
        if($user->type==4 || $user->type==3){
            return view('clients.index', ['user'=>$user]);
        } else {
            \Session::forget('building_id');
            return view('build');
        }
    }

    public function userLogout() {
        Auth::logout();
        return redirect('/');
    }
}
