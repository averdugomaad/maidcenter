<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unity extends Model
{
    protected $fillable = ['label', 'type', 'floor', 'tower', 'prorateo', 'assign_rol', 'building_id'];

	const TYPES = [
		1 => 'Departamento',
		2 => 'Oficina',
		3 => 'Box',
		4 => 'Local',
		5 => 'Estacionamiento',
		6 => 'Bodega'
	];

	public function extensions()
	{
		return $this->hasMany('App\UnityExtension');
	}

	public function getTypeLabelAttribute()
    {
        return Self::TYPES[$this->type];
    }
}
