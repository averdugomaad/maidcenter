<?php

namespace App\Imports;

use App\Unity;
use Maatwebsite\Excel\Concerns\ToModel;

class UnitiesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Unity([
            'label' =>$row[0],
			'type'=>$row[1],
			'floor'=>$row[2],
			'tower'=>$row[3],
			'prorateo'=>$row[4],
			'assign_rol'=>$row[5], 
			'building_id'=>\Session::get('building_id')
        ]);
    }
}
