<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Providers extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'type', 'building_id'];

	public function getTypeLabelAttribute()
	{
		$array = ['', 'Remuneraciones y Honorarios', 'Mantenciones', 'Reparaciones', 'Consumos Básicos', 'Otros Gastos', 'Insumos', 'Otros Ingresos'];

		return $array[$this->type];
	}
}
