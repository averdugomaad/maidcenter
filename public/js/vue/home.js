new Vue({
	el:'#wrapper',
	created: function(){
		this.getList()
	},
	data: {
		lists: [],
		data: {},
		url: '/edificios',
		file: ''
	},
	methods: {
		getList(){
			axios.get(this.url).then( response => {
				this.lists = response.data
			})
		},
		handleFileUpload(){
			this.file = this.$refs.file.files[0];
      	},
		submit(){
		    let formData = new FormData();
			this.data.type = 1;
			this.data.status = 1;
		    formData.append('file', this.file);
			formData.append('data',JSON.stringify(this.data));

		    axios.post(this.url, formData, { headers: {'Content-Type': 'multipart/form-data'} }
			).then( r => {
				this.getList();
				this.data = {};
				$("#createBuildingModal").modal('hide');
				toastr.success('Producto Guardado Exitosamente');
				this.file = '';
				this.errors = [];
			})
			.catch( error =>{
				this.errors = error.response.data.errors
			});

		},
	}
})
