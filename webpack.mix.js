const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.scripts([
 	'resources/js/assets/toastr.js',
 	'resources/js/assets/vue.js',
 	'resources/js/assets/axios.js',
 	'resources/js/assets/home.js',
],'public/js/home.js');
